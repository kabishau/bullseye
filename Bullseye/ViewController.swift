//
//  ViewController.swift
//  Bullseye
//
//  Created by Aleksey Kabishau on 0801..17.
//  Copyright © 2017 Dreamline. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: properties
    
    // for keeping track of current value of slider
    var currentValue = 0
    
    // for randomly generated number
    var targetValue = 0
    
    // to keep track score and round
    var score = 0
    var round = 0
    
    
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!

    //MARK: methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewRound()
        updateLabels()
        
        // customization of the slider in code
        // creating image for normal state of the slider; also works without unwrapping - why?
        let thumbImageNormal = UIImage(named: "SliderThumb-Normal")
        //adding image to the slider
        slider.setThumbImage(thumbImageNormal, for: .normal)
        // image for highlighted state; also works without unwrapping - why?
        let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")
        // adding image to the slider
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        // customizing the slider's lines - resizable image; is it possible to do the same through the UI in Xcode
        // trackLeftImage - should it be optional?
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = UIImage(named: "SliderTrackLeft")!
        let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        let trackRightImage = UIImage(named: "SliderTrackRight")!
        let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightResizable, for: .normal)
        

    }
    
    // do I really need this method?
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("rotated")
    }
    
    // sender is changed from Any to UISlider - no need to cast?
    @IBAction func sliderMoved(_ sender: UISlider) {
        
        // lroundf - take a float and make an integer
        currentValue = lroundf(sender.value)

    }
    
    // method connected to button HitMe and calling in slider
    @IBAction func showAlert(_ sender: Any) {
        
        let difference = abs(targetValue - currentValue)
        //calculation of point depending on results
        var points = 100 - difference
        // for alert title
        let title: String
        
        if difference == 0 {
            title = "Perfect"
            points += 100
        } else if difference < 5 {
            title = "You almost got it!"
            if difference == 1 {
                points += 50
            }
        } else if difference < 10 {
            title = "Pretty good"
        } else {
            title = "Not even close"
        }
        
        // message for alert controller
        let message = "The value of the slider is \(currentValue)\n The target value is \(targetValue)\nYou scored \(points)"
        // updating score
        score += points
        
        // instance of alert controller
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // creating additional action for alert controller
        // why do I need self. here? What does it mean [weak self]? that brings ? after self.
        let actionItem = UIAlertAction(title: "OK", style: .default) { [weak self] action in
            self?.startNewRound()
            self?.updateLabels()
        }
        
        // adding action to alert controller
        alertController.addAction(actionItem)
        
        // presenting alert controller
        present(alertController, animated: true, completion: nil)
        

    }
    
    @IBAction func startOver(_ sender: Any) {
        startNewGame()
        updateLabels()
        
    }
    
    func startNewGame() {
        score = 0
        round = 0
        startNewRound()
    }
    
    
    // resetting value and positions every round
    func startNewRound() {
        // random number - why + 1?
        targetValue = Int(arc4random_uniform(100)) + 1
        
        // reloading current value to 50 every round
        currentValue = 50
        
        // setting slider position in the middle every round
        slider.value = Float(currentValue)
        
        // updating roundlabel
        round += 1
    }
    
    // method used in labels upgrading
    func updateLabels() {
        targetLabel.text = String(targetValue)
        roundLabel.text = String(round)
        scoreLabel.text = String(score)
    }
    


}

