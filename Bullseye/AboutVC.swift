//
//  AboutVC.swift
//  Bullseye
//
//  Created by user128830 on 8/4/17.
//  Copyright © 2017 Dreamline. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // creating the url
        if let url = Bundle.main.url(forResource: "BullsEye", withExtension: "html") {
            if let htmlData = try? Data(contentsOf: url) {
                let baseUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
                webView.load(htmlData, mimeType: "text/html", textEncodingName: "URF-8", baseURL: baseUrl)
            }
        }

    }
    
    @IBAction func close(_ sender: Any) {
        
        // dismiss method for segue
        dismiss(animated: true, completion: nil)
    }
    



}
